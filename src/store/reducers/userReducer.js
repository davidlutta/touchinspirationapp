import {
    GET_USER,
    GET_USERS,
    UPDATE_USER
} from "../actions/types";

const initialState = {
    users: [],
    user: {}
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USERS: {
            return {
                ...state,
                users: action.payload
            };
        }
        case GET_USER: {
            return {
                ...state,
                user: action.payload
            };
        }
        case UPDATE_USER: {
            return {
                ...state,
                users: state.users.map(
                    user =>
                        user.id === action.payload.id
                            ? (user = action.payload) : user
                )
            };
        }
        default:
            return state;
    }
};
export default userReducer;
