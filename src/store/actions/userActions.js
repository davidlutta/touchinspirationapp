import {GET_USER, GET_USERS, UPDATE_USER} from "./types";
// import userApi from "../../services/UsersApi";
import axios from 'axios';

// const client = new userApi
export const getUsers = () => async dispatch => {
    const res = await axios.get('https://ti-react-test.herokuapp.com/users');
    dispatch({
        type: GET_USERS,
        payload: res.data
    });
};

export const getUser = (id) => async dispatch => {
    const res = await axios.get(`https://ti-react-test.herokuapp.com/users/${id}`);
    dispatch({
        type:GET_USER,
        payload: res.data
    });
};

export const updateUser = (user, id) => async dispatch => {
    const res = await axios.patch(
        `https://ti-react-test.herokuapp.com/users/${id}`,
        {
            name: user.name,
            bio: user.bio,
            email: user.email,
            occupation: user.occupation
        }
    );
    dispatch({
        type: UPDATE_USER,
        payload: res.data
    });
};
