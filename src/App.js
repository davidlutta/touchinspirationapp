import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import UserList from "./components/userList/userList";
import User from "./components/user/user";
import UpdateForm from "./components/updateUser/updateForm";
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
      <BrowserRouter>
          <Navbar/>
          <div className="App container">
              <Switch>
                  <Route exact path={'/'} component={UserList}/>
                  <Route exact path={'/user/:id'} component={User}/>
                  <Route exact path={'/user/edit/:id'} component={UpdateForm}/>
              </Switch>
          </div>
      </BrowserRouter>
  );
}
export default App;
