import React from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import {getUsers} from "../../store/actions/userActions";
import "./index.css";

class UserList extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const {users} = this.props;
        const userList = users.length ?
            (
                users.map(user => {
                    return (
                        <div className={"cont card"} key={user.id}>
                            <p className="btn btn-floating red lighten-1 left icon">{user.name[0]}</p>
                            <Link to={'/user/' + user.id}>
                                <div className="content">
                                    <div>
                                        <span className="card-title red-text">{user.name}</span>
                                    </div>
                                    <p>{user.bio}</p>
                                </div>
                            </Link>
                        </div>
                    );
                })
            )
            : (
                <div>
                    <h2 className={'title center grey-text note-date'}>Loading...</h2>
                </div>
            );
        return (

            <div>
                <h3>User List</h3>
                <div>
                    {userList}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users.users
    };
};

function mapDispatchToProps(dispatch) {
    return {
        getUsers: () => dispatch(getUsers())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
