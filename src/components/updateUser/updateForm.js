import React, {Component} from 'react';
import {updateUser} from "../../store/actions/userActions";
import {connect} from "react-redux";
import "./index.css";

class UpdateForm extends Component {
    state = {
        name: '',
        nameError: '',
        email: '',
        emailError: '',
        occupation: '',
        occupationError: '',
        bio: '',
        bioError: ''
    };

    validate = () => {
        let nameError = '';
        if (!this.state.name) {
            nameError = 'Please enter your Name';
            this.setState({
                emailError: null,
                occupationError: null,
                bioError: null,
                nameError: nameError
            });
            return false;
        }
        let emailError = '';
        if (!this.state.name) {
            emailError = 'Please enter your Email';
            this.setState({
                emailError: emailError,
                occupationError: null,
                bioError: null,
                nameError: null
            });
            return false;
        }
        let occupationError = '';
        if (!this.state.name) {
            occupationError = 'Please enter your Occupation';
            this.setState({
                emailError: null,
                occupationError: occupationError,
                bioError: null,
                nameError: null
            });
            return false;
        }
        let bioError = '';
        if (!this.state.name) {
            bioError = 'Please enter a Bio';
            this.setState({
                emailError: null,
                occupationError: null,
                bioError: bioError,
                nameError: null
            });
            return false;
        }
        return true;
    };
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        let id = this.props.match.params.id;
        if (isValid) {
            this.props.updateUser(this.state, id);
            this.props.history.push('/');
        }
    };

    render() {
        return (
            <div>
                <form className={"myCard"} onSubmit={this.handleSubmit}>
                    <h5 className={"grey-text text-darken-3"}>Update User</h5>
                    <br/>
                    <div className="row">
                        <div className="input-field">
                            <label htmlFor="name">Name</label>
                            <input type="text" id={"name"} onChange={this.handleChange}/>
                        </div>
                        <div className="red-text center">
                            {this.state.nameError}
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field">
                            <label htmlFor="email">Email</label>
                            <input type="email" id={"email"} onChange={this.handleChange}/>
                        </div>
                        <div className="red-text center">
                            {this.state.emailError}
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field">
                            <label htmlFor="occupation">occupation</label>
                            <input type="text" id={"occupation"} onChange={this.handleChange}/>
                        </div>
                        <div className="red-text center">
                            {this.state.occupationError}
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field">
                            <label htmlFor="bio">bio</label>
                            <input type="text" id={"bio"} onChange={this.handleChange}/>
                        </div>
                        <div className="red-text center">
                            {this.state.bioError}
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field">
                            <button className="btn red darken z-depth-0">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateUser: (user, id) => dispatch(updateUser(user, id))
    };
}

export default connect(null, mapDispatchToProps)(UpdateForm);
