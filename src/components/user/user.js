import React, {Component} from 'react';
import {getUser} from "../../store/actions/userActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import './index.css';
import moment from "moment";

class User extends Component {
    componentDidMount() {
        let id = this.props.match.params.id;
        this.props.getUser(id);
    }

    render() {
        const {user} = this.props;
        return (
            <div className={'container'}>
                <div className="center">
                    <div className="myCard">
                        <h3 className={'title'}>{user.name}</h3>
                        <p>{user.email}</p>
                        <p>{user.bio}</p>
                        <div className={"grey-text note-date"}>Created at {moment(user.created_at).fromNow()}</div>
                        <p className={"grey-text note-date"}>Last Update {moment(user.updated_at).fromNow()}</p>
                        <Link to={'/user/edit/' + user.id}>
                            <button className="btn red darken">Update User</button>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user:state.users.user
    };
};
function mapDispatchToProps(dispatch) {
    return {
        getUser: (id) => dispatch(getUser(id))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(User);
